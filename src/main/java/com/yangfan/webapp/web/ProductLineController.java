package com.yangfan.webapp.web;

import com.yangfan.webapp.domain.ProductLine;
import com.yangfan.webapp.domain.ProductLineRepository;
import com.yangfan.webapp.service.ProductLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.core.DummyInvocationUtils.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/product-lines")
public class ProductLineController {

    private ProductLineRepository productLineRepository;
    @Autowired
    private ProductLineService productLineService;

    public ProductLineController(ProductLineRepository productLineRepository) {
        this.productLineRepository = productLineRepository;
    }

    @GetMapping
    public ResponseEntity getProductLines(@RequestParam(defaultValue = "0") Integer page,
                                          @RequestParam(defaultValue = "3") Integer size,
                                          @RequestParam(defaultValue = "description") String sort,
                                          @RequestParam(defaultValue = "ASC") String direction){

        Pageable pageFinal = productLineService.pagedirection(page, size, sort, direction);

        Page<ProductLine> pages = productLineRepository.findAll(pageFinal);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(pages.getContent());
    }


    @PostMapping
    public ResponseEntity createProductLines(@RequestBody ProductLine productLine){
        ProductLine saveProductLine = productLineRepository.save(productLine);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(saveProductLine);
    }
}

