package com.yangfan.webapp.web;

import com.yangfan.webapp.contract.CreateProductRequest;
import com.yangfan.webapp.domain.Product;
import com.yangfan.webapp.domain.ProductLine;
import com.yangfan.webapp.domain.ProductLineRepository;
import com.yangfan.webapp.domain.ProductRepository;
import com.yangfan.webapp.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;


    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping("/products")
    public ResponseEntity getProduct(){
        List<Product> all = productRepository.findAll();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(all);
    }

    @PostMapping("/products")
    public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest productRequest){
        Product product = productService.createProduct(productRequest);
        Product saveProduct = productRepository.save(product);
        return ResponseEntity
                .created(linkTo(methodOn(ProductController.class).getProduct()).toUri())
                .body(saveProduct);
    }
}

