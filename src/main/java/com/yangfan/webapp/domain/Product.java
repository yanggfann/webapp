package com.yangfan.webapp.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @Column(name = "productCode", nullable = false, length = 15)
    private String code;

    @Column(name = "productName", nullable = false, length = 70)
    private String name;

    @ManyToOne
    @JoinColumn(name = "productLine")
    private ProductLine productLine;

    @Column(name = "productScale", nullable = false, length = 10)
    private String scale;

    @Column(name = "productVendor", nullable = false, length = 50)
    private String vendor;

    @Column(name = "productDescription", nullable = false, columnDefinition = "text")
    private String description;

    //????
//    @Column(name = "quantityInStock", nullable = false, columnDefinition = "smallint", length = 6)
    @Column(name = "quantityInStock", nullable = false)
    private Short quantityInStock;

//    @Column(name = "buyPrice", nullable = false, precision = 10, scale = 2)
    @Column(name = "buyPrice", nullable = false)
    private BigDecimal buyPrice;

    @Column(name = "MSRP", nullable = false)
    private BigDecimal MSRP;

    public Product() {
    }

    public Product(String code, String name, ProductLine productLine, String scale, String vendor, String description, Short quantityInStock, BigDecimal buyPrice, BigDecimal MSRP) {
        this.code = code;
        this.name = name;
        this.productLine = productLine;
        this.scale = scale;
        this.vendor = vendor;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.MSRP = MSRP;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getMSRP() {
        return MSRP;
    }

}
