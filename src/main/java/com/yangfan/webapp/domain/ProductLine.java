package com.yangfan.webapp.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "productlines")
public class ProductLine {

    @Id
    @Column(name = "productLine", nullable = false, length = 50)
    private String name;

    @Column(name = "textDescription")
    @Size(max = 4000)
    private String description;

    public ProductLine() {
    }

    public ProductLine(String name, @Size(max = 4000) String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
}
