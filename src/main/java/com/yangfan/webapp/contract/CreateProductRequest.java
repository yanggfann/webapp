package com.yangfan.webapp.contract;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Component
public class CreateProductRequest {

    @NotNull
    private String code;

    @NotNull
    private String name;

    @NotNull
    private String productLine;

    @NotNull
    private String scale;

    @NotNull
    private String vendor;

    @NotNull
    private String description;

    @NotNull
    private Short quantityInStock;

    @NotNull
    private BigDecimal buyPrice;

    @NotNull
    @JsonProperty("MSRP")
    private BigDecimal msrp;

    public CreateProductRequest() {
    }

    public CreateProductRequest(@NotNull String code, @NotNull String name, @NotNull String productLine, @NotNull String scale, @NotNull String vendor, @NotNull String description, @NotNull Short quantityInStock, @NotNull BigDecimal buyPrice, @NotNull BigDecimal msrp) {
        this.code = code;
        this.name = name;
        this.productLine = productLine;
        this.scale = scale;
        this.vendor = vendor;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.msrp = msrp;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getProductLine() {
        return productLine;
    }

    public String getScale() {
        return scale;
    }

    public String getVendor() {
        return vendor;
    }

    public String getDescription() {
        return description;
    }

    public Short getQuantityInStock() {
        return quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public BigDecimal getmsrp() {
        return msrp;
    }
}
