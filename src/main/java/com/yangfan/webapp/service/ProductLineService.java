package com.yangfan.webapp.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ProductLineService {
    private Pageable pageRequest;

    public Pageable pagedirection(Integer page, Integer size, String sort, String direction) {

    if(direction.equals("desc")){
        pageRequest = PageRequest.of(page, size, Sort.by(sort).descending());
    }else{
        pageRequest = PageRequest.of(page, size, Sort.by(sort).ascending());
    }
    return pageRequest;
  }
}
