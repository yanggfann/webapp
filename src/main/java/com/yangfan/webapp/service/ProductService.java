package com.yangfan.webapp.service;

import com.yangfan.webapp.contract.CreateProductRequest;
import com.yangfan.webapp.domain.Product;
import com.yangfan.webapp.domain.ProductLine;
import com.yangfan.webapp.domain.ProductLineRepository;
import com.yangfan.webapp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    private ProductLineRepository productLineRepository;

    public Product createProduct(CreateProductRequest request){
        Optional<ProductLine> productLine = productLineRepository.findById(request.getProductLine());
        Product product = new Product(request.getCode(), request.getName(), productLine.get(),
                request.getScale(), request.getVendor(), request.getDescription(),
                request.getQuantityInStock(), request.getBuyPrice(), request.getmsrp());
        return product;
    }
}
